import numpy as np
from PIL import Image

img = Image.open('picture.jpg').convert('RGB')
h,w = img.size
x = np.array(img)
for i in range(len(x)):
    for j in range(len(x[i])):
        r,g,b = x[i][j]
        x[i][j][0] = g
        x[i][j][1] = b
        x[i][j][2] = r

img = Image.fromarray(x, 'RGB')
img.save('picture_1.jpg')



img = Image.open('picture.jpg').convert('RGB')
h,w = img.size
x = np.array(img)
for i in range(len(x)):
    for j in range(len(x[i])):
        r,g,b = x[i][j]
        r =70+ int(r * 71) / 256
        g =70+ int(g * 71) / 256
        b =70+ int(b * 71) / 256
        x[i][j][0] = r
        x[i][j][1] = g
        x[i][j][2] = b

img = Image.fromarray(x, 'RGB')
img.save('picture_2.jpg')




img = Image.open('picture.jpg').convert('RGB')
h,w = img.size
x = np.array(img)
for i in range(len(x)):
    for j in range(len(x[i])):
        r,g,b = x[i][j]
        r = int(r)
        g = int(g)
        b = int(b)
        z = (r+g+b)//3
        x[i][j][0] = z
        x[i][j][1] = z
        x[i][j][2] = z

img = Image.fromarray(x, 'RGB')
img.save('picture_3.jpg')