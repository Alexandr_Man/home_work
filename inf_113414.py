def N(x1,x2,x3):
    if x2 == x3:
        k = 1
    else:
        k = 0
    if x1 == 1 and k == 0:
        return 0
    else:
        return 1

def ili(z1,z2):
    if z1 == 1 or z2 == 1:
        return 1
    else:
        return 0
def iii(z1,z2):
    if z1 == 1 and z2 == 1:
        return 1
    else:
        return 0

def P(x1,x2,x3):
    k1 = iii(x1, x2)
    k2 = iii(x1, x3)
    k3 = iii(x2, x3)
    k12 = ili(k1, k2)
    return ili(k12, k3)

def B(x1,x2,x3):
    b1 = ili(x1, x2)
    b2 = ili(b1, x3)
    n1 = N(x1,x2,x3)
    n2 = N(x2,x1,x3)
    n3 = N(x3,x1,x2)
    b3 = iii(b2,n1)
    b4 = iii(n2,n3)
    return iii(b3,b4)

mas = []
for i in range(7):
    a = list(map(int,input().split()))
    mas.append(a)

for i in range(7):
    s = str(mas[i][0]) + ' '
    x0 = 0
    b = B(mas[i][0],mas[i][1],0)
    p = P(mas[i][0],mas[i][1], 0)
    s += str(b) + ' '
    for j in range(1, len(mas[i]) - 1):
        s += str(B(mas[i][j], mas[i][j + 1], p)) + ' '
        p = P(mas[i][j],mas[i][j + 1], p)
    print(s)