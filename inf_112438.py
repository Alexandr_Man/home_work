n = int(input())
k1 = 0
k2 = 0
k3 = 0
k4 = 0
d1 = [1000000, 1000000]
d2 = [1000000, 1000000]
d3 = [1000000, 1000000]
d4 = [1000000, 1000000]
for i in range(n):
    x, y = map(int,input().split())
    if x != 0 and y != 0:
        if x < 0 and y < 0:
            k3 += 1
            if abs(x) < min(abs(d3[0]),abs(d3[1])) or abs(y) < min(abs(d3[0]),abs(d3[1])):
                d3 = [x, y]
        elif x > 0 and y > 0:
            k1 += 1
            if abs(x) < min(abs(d1[0]),abs(d1[1])) or abs(y) < min(abs(d1[0]),abs(d1[1])):
                d1 = [x, y]
        elif x < 0 and y > 0:
            k2 += 1
            if abs(x) < min(abs(d2[0]),abs(d2[1])) or abs(y) < min(abs(d2[0]),abs(d2[1])):
                d2 = [x, y]
        else:
            k4 += 1
            if abs(x) < min(abs(d4[0]),abs(d4[1])) or abs(y) < min(abs(d4[0]),abs(d4[1])):
                d4 = [x, y]

mas = [[k1, 1, d1], [k2, 2, d2], [k3, 3, d3], [k4, 4, d4]]
mas.sort()
mas.reverse()
if mas[0][0] != mas[1][0]:
    print('K = ', mas[0][1], sep= '')
    print('M = ', mas[0][0], sep= '')
    if mas[0][1] == 1:
        print('A = ', '(', d1[0], ', ', d1[1], ')', sep = '')
        print('R = ', min(abs(d1[0]), abs(d1[1])), sep= '')
    elif mas[0][1] == 2:
        print('A = ', '(', d2[0], ', ', d2[1], ')', sep = '')
        print('R = ', min(abs(d2[0]), abs(d2[1])), sep= '')
    elif mas[0][1] == 3:
        print('A = ', '(', d3[0], ', ', d3[1], ')', sep = '')
        print('R = ', min(abs(d3[0]), abs(d3[1])), sep= '')
    else:
        print('A = ', '(', d4[0], ', ', d4[1], ')', sep='')
        print('R = ', min(abs(d4[0]), abs(d4[1])), sep= '')
else:
    r = 1000000
    tek = 0
    k = 5
    for i in range(len(mas)):
        if mas[i][0] == mas[0][0]:
            if min(abs(mas[i][2][0]), abs(mas[i][2][1])) < r:
                r = min(abs(mas[i][2][0]), abs(mas[i][2][1]))
                k = mas[i][1]
                m = mas[i][0]
                a = '(' +str(mas[i][2][0]) + ', ' + str(mas[i][2][0]) + ')'
            elif min(abs(mas[i][2][0]), abs(mas[i][2][1])) == r:
                if mas[i][1] < k:
                    r = min(abs(mas[i][2][0]), abs(mas[i][2][1]))
                    k = mas[i][1]
                    m = mas[i][0]
                    a = '(' + str(mas[i][2][0]) + ', ' + str(mas[i][2][1]) + ')'
    print('K = ', k, sep= '')
    print('M = ', m, sep= '')
    print('A = ', a, sep= '')
    print('R = ', r, sep= '')