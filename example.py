with open('input.txt') as f:
    text = f.read()
    a = text.split('\n')[1]
    mas = list(map(int,a.split()))

k = 1
m = mas[0]
real = max(mas)
tek = 1
for i in range(1, len(mas)):
    if m == real:
        break
    if mas[i] <= m:
        tek += 1
    elif mas[i] > m:
        m = mas[i]
        if tek > k:
            k = tek
        tek = 1

with open('output.txt', 'w') as f:
    f.write(str(k))