from math import sin
V, a, K = map(float,input().split())
pi = 3.1415926535
si2 = sin(a * pi / 90)
len = 0
while True:
    len += V**2 *si2 / 10
    V /= K ** 0.5
    if V < 0.01:
        break
print(round(len,2))