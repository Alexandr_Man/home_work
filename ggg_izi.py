n = int(input())
s = 0
for i in range(n):
    x, y = map(int,input().split())
    x += y
    s += x // 10
    s *= 10
    s += x % 10
print(s)
