n,m,k = map(int,input().split())
mas_koor = []
for i in range(k):
    a = input().split()
    a[0] = int(a[0]) -1
    a[1] = int(a[1]) - 1
    mas_koor.append(a)
mas = []
temp = [0 for x in range(m)]
for i in range(n) :
    mas.append(temp.copy())

for i in range(n):
    for j in range(m):
        if [i-1,j-1] in mas_koor:
            mas[i][j]+=1
        if [i,j-1] in mas_koor:
            mas[i][j]+=1
        if [i+1,j-1] in mas_koor:
            mas[i][j]+=1
        if [i-1,j] in mas_koor:
            mas[i][j]+=1
        if [i+1,j] in mas_koor:
            mas[i][j]+=1
        if [i-1,j+1] in mas_koor:
            mas[i][j]+=1
        if [i,j+1] in mas_koor:
            mas[i][j]+=1
        if [i+1,j+1] in mas_koor:
            mas[i][j]+=1


for i in range(len(mas_koor)):
    x = int(mas_koor[i][0])
    y = int(mas_koor[i][1])
    mas[x][y] = '*'

for i in range(len(mas)):
    v = ''
    for j in range(len(mas[i])):
        v += str(mas[i][j])
        v += ' '
    print(v)
