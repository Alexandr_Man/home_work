import pickle
import pickle
import networkx as nx
import matplotlib.pyplot as plt
from datetime import datetime
import operator

if __name__ == '__main__':
    with open('vk_friends.pickle', 'rb') as f:
        vk_friends = pickle.load(f)

    user_id = 101264162

    users = vk_friends.keys()

    k = {}
    # for key in vk_friends.keys():
    #     k[key] = []
    #     for user in vk_friends[key]:
    #         if user['id'] in users:
    #             k[key].append(user['id'])
    # import pprint
    # pprint.pprint(vk_friends[user_id])
    # exit()

    p = {}

    for key in vk_friends.keys():
        b = False
        for key1 in vk_friends.keys():
            if b:
                break
            for user in vk_friends[key1]:
                if user['id'] == key:
                    p[key] = user
                    b = True
                    break


    # import pprint
    # pprint.pprint(p)
    # print(vk_friends.keys())

    # exit()
    def get_f_l(key):
        try:
            return p[key]["first_name"] + "_" + p[key]["last_name"]
        except KeyError:
            return key

    for key in vk_friends.keys():
        k[get_f_l(key)] = []
        for user in vk_friends[key]:
            if user['id'] in users:
                k[get_f_l(key)].append(get_f_l(user['id']))


    # for key in vk_friends.keys():
    #     # if len(vk_friends[key]):
    #     k[vk_friends[key][0]["first_name"] + "_" + vk_friends[key][0]["last_name"]] = []
    #
    #     for user in vk_friends[key]:
    #         if user['id'] in users:
    #             # if len(vk_friends[key]) and len(vk_friends[user['id']]):
    #             k[vk_friends[key][0]["first_name"] + "_" + vk_friends[key][0]["last_name"]].append(\
    #                 vk_friends[user['id']][0]["first_name"] + "_" + vk_friends[user['id']][0]["last_name"])



    with open("graph_vk.json", "w", encoding='utf8') as f:
        f.write('{\n"nodes": [\n')
        m = []
        for key in k.keys():
            m.append('{{"id": "{0}", "group": 5}}'.format(key))
        f.write(",\n".join(m))
        f.write('],\n"links": [\n')
        m = []
        for key in k.keys():
            for el in k[key]:
                m.append('{{"source": "{0}", "target": "{1}", "value": 1}}'.format(key, el))
        f.write(",\n".join(m))
        f.write("\n]\n}")


