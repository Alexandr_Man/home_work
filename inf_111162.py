def sort_ubiv(a):
    for j in range(1, len(a)):
        for i in range(len(a)-j):
            if int(a[i]) < int(a[i+1]):
                a[i],a[i+1] = a[i+1],a[i]
    return a
 
def sort_vozr(a):
    for j in range(1, len(a)):
        for i in range(len(a)-j):
            if int(a[i]) > int(a[i+1]):
                a[i],a[i+1] = a[i+1],a[i]
    return a
 
 
a = list(map(int,input().split()))
b = list(map(int,input().split()))
a = sort_ubiv(a)
b = sort_vozr(b)
s = 0
for i in range(len(a)):
    s+=a[i]*b[i]
print(s)
