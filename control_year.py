import numpy as np
import random as rnd
from matplotlib import pyplot as plt
from matplotlib import animation
mas = []
for i in range(100):
    m = []
    for j in range(4):
        s = rnd.random()
        s *= 2
        if j == 2 or j == 3:
            s -= 1
        m.append(s)
    mas.append(m)
for i in range(len(mas)):
    mas[i].append(0)
mas[0][4] = 1
fig = plt.figure()
ax = plt.axes(xlim=(0, 2), ylim=(0, 2))
line, = ax.plot([], [],'o', ms = 3)

def init():
    line.set_data([], [])
    return line,

def opr_x(i):
    mas_x = []
    for j in range(len(mas)):
        x = mas[j][0]
        Vx = mas[j][2]
        x += Vx  / 200
        if x > 2:
            x-=2
        if x < 0:
            x+=2
        mas[j][0] = x
        mas_x.append(x)
    return mas_x

def SearchDistance(x1,y1,x2,y2):#вычисляет расстояние между точками заданными координатами
    distance = ((x1-x2)**2+(y1-y2)**2)**0.5
    return distance

def opr_y(i):
    mas_y = []
    for j in range(len(mas)):
        y = mas[j][1]
        Vy = mas[j][3]
        y += Vy  / 200
        if y > 2:
            y-=2
        if y < 0:
            y+=2
        mas[j][1] = y
        mas_y.append(y)
    return mas_y

def get_color(i,x,y):
    for j in range(100):
        for t in range(100):
            if SearchDistance(x[j],y[j],x[t],y[t]) <= 0.05 and (mas[j][4] == 1 or mas[t][4] == 1):
                mas[j][4] = 1
                mas[t][4] = 1
    massiv = []
    for l in range(len(mas)):
        massiv.append(mas[l][4])
    return massiv
mas_x = []
mas_y = []

def animate(i):
    x = opr_x(i)
    y = opr_y(i)
    massiv = get_color(i,x,y)
    kol_zaraza = massiv.count(1)
    if kol_zaraza == 100:
        pass
    else:
        mas_y.append(kol_zaraza)
    line.set_data(x, y)
    return line,

anim = animation.FuncAnimation(fig, animate, init_func=init,
                               frames=200, interval=20, blit=True)


plt.show()

for i in range(len(mas_y)):
    mas_x.append(i)
fig2= plt.figure()
plt.plot(mas_x,mas_y)
plt.show()
#Я не разобрался как поменять цвет точек, поэтому построил график количества зараженных от i
#Однако с графиком что-то странное