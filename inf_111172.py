def primes(n):
    a = []
    for i in range(n):
        a.append(i)
    a[1] = 0
    m = 2
    while m < n:
        if a[m] != 0:
            j = m * 2
            while j < n:
                a[j] = 0
                j = j + m
        m += 1
    b = []
    for i in a:
        if a[i] != 0:
            b.append(a[i])
    return b

n = int(input())
mas = primes(n**2)
print(mas[n-1])