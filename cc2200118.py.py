import socket
from threading import Thread

def inputer():
    while True:
        data = sock.recv(1024)
        print(data.decode())

def outputer():
    while True:
        data = input()
        sock.send(data.encode())

sock = socket.socket()
sock.connect(('localhost', 9091))

thread_input = Thread(target=inputer)
thread_output = Thread(target=outputer)

thread_input.start()
thread_output.start()

thread_input.join()
thread_output.join()