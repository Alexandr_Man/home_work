n = int(input())
mas = []
for i in range(n):
    mas.append(int(input()))
mas.sort()
if len(mas) == 2:
    print(mas[0] + mas[1])
else:
    if (mas[0] + mas[1]) % 2 == 0:
        print(mas[0] + mas[1])
    else:
        s1 = 0
        for i in range(2, len(mas)):
            if (mas[0] + mas[i]) % 2 == 0:
                s1 = mas[0] + mas[i]
                break
        s2 = 0
        for i in range(2, len(mas)):
            if (mas[1] + mas[i]) % 2 == 0:
                s2 = mas[1] + mas[i]
                break
        if s1 == 0:
            print(s2)
        elif s2 == 0:
            print(s1)
        else:
            print(min(s1,s2))