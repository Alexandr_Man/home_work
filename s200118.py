import socket

sock = socket.socket()
sock.bind(('', 9090))
sock.listen(1)
conn, addr = sock.accept()

print('connected:', addr)

while True:
    data = conn.recv(1024)
    data_s = data.decode()
    if data_s == 'exit':
        break
    print(data_s)
    conn.send(data_s.upper().encode())

conn.close()