n = int(input())
st = ['.' for x in range(n)]
mas = []
for i in range(n):
    mas.append(st.copy())

for i in range(n):
    for j in range(n):
        mas[i][0] = '0'
        mas[i][n - 1] = '2'

for i in range(n):
    for j in range(n):
        if j == n - i - 1:
            mas[i][j] = '1'

for i in range(n):
    for j in range(1, n - 1):
        if mas[i][j - 1] == '0' and mas[i][j] != '1':
            mas[i][j] = '0'
        elif mas[i][j - 1] == '1' or '2' and mas[i][j] != '1':
            mas[i][j] = '2'

for i in range(n):
    print(' '.join(mas[i]))

