n = int(input())
st = ['.' for x in range(n)]
mas = []
for i in range(n):
    mas.append(st.copy())

for i in range(n):
    for j in range(n):
        mas[i][0] = str(i)
        if j == i:
            mas[i][j] = '0'

for i in range(n):
    for j in range(1, n):
        if mas[i][j] != '0':
            mas[i][j] = str(int(mas[i][j - 1]) - 1)

for i in range(n):
    for j in range(n):
        mas[i][j] = str(abs(int(mas[i][j])))
    print(' '.join(mas[i]))