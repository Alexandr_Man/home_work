def init_1():
    n,m,g = map(int,input().split())
    return n,m,g

def init_2(n):
    mas = []
    for i in range(n):
        a = list(input())
        mas.append(a)
    return mas

def ratio(x):
    dict = {
        's':'d',
        'S':'D',
        'n':'u',
        'N':'U',
        'e':'r',
        'E':'R',
        'w':'l',
        'W':'L'
    }
    y = dict[x]
    return y

def rename(a):
    for i in range(len(a)):
        for j in range(len(a[i])):
            a[i][j] = ratio(a[i][j])
    return a

def prosto(mas):
    new_mas = []
    for i in range(len(mas)):
        a = [0] * len(mas[i])
        new_mas.append(a)
    return new_mas

def mine(mas,mas_0):
    ind = 0
    time = 1
    res = []
    for i in range(len(mas)):
        for j in range(len(mas[i])):
            n, m = i, j
            if mas_0[i][j] == 0:
                ind = 0
                d = time
                while ind != 1:
                    time += 1
                    if mas[n][m].lower() == 'd':
                        mas_0[n][m] = time
                        if mas_0[n+1][m] == 0:
                            n += 1
                        else:
                            if d < mas_0[n+1][m]:
                                dlina = time - mas_0[n+1][m] + 1
                                res.append(dlina)
                            break
                    elif mas[n][m].lower() == 'u':
                        mas_0[n][m] = time
                        if mas_0[n-1][m] == 0:
                            n -= 1
                        else:
                            if d < mas_0[n-1][m]:
                                dlina = time - mas_0[n-1][m] + 1
                                res.append(dlina)
                            break
                    elif mas[n][m].lower() == 'l':
                        mas_0[n][m] = time
                        if mas_0[n][m-1] == 0:
                            m -= 1
                        else:
                            if d < mas_0[n][m-1]:
                                dlina = time - mas_0[n][m-1] + 1
                                res.append(dlina)
                            break
                    elif mas[n][m].lower() == 'r':
                        mas_0[n][m] = time
                        if mas_0[n][m+1] == 0:
                            m += 1
                        else:
                            if d < mas_0[n][m+1]:
                                dlina = time - mas_0[n][m+1] + 1
                                res.append(dlina)
                            break
    return res

def finding_cycle(mas):
    mas_0 = prosto(mas)
    print(sum(mine(mas,mas_0)))

n,m,g = init_1()
mas = init_2(n)
mas = rename(mas)
finding_cycle(mas)