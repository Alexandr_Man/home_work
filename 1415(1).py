def init():
    N = int(input())
    stations = []
    for i in range(N):
        string = input().split()
        string[0] = int(string[0])
        string[1] = int(string[1])
        string[2] = int(string[2])
        stations.append(string)
    M = int(input())
    lines = []
    for i in range(M):
        string = list(map(int,input().split()))
        lines.append(string)
    return stations, lines

def ComputingRadius(r0,n):
    radius = r0 * (10**(0.2*n))
    radius = round(radius,3)
    return radius

def SearchDistance(x1,y1,x2,y2):
    distance = ((x1-x2)**2+(y1-y2)**2)**0.5
    return distance

def DefinitionLevel(r0, distance):
    if distance <= r0:
        return 0
    else:
        decebels = 10 * math.log10(distance / r0)
        level = math.ceil(decebels / 2)
        return level

def ChoiseStation(stations, x0, y0):
    signal_levels = []
    for i in range(len(stations)):
        distance = SearchDistance(x0,y0,stations[i][0],stations[i][1])
        level = DefinitionLevel(stations[i][2], distance)
        signal_levels.append(level)
    minimum = 20
    index= 0
    for i in range(len(signal_levels)):
        if signal_levels[i] < minimum:
            minimum = signal_levels[i]
            index = i
    z = 0
    for i in range(len(signal_levels)):
        if signal_levels[i] == minimum:
            z+=1
    if z == 1:
        return stations[index]
    else:
        x = 'zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz'
        index = 0
        for i in range(len(signal_levels)):
            if stations[i][3] < x and signal_levels[i] == minimum:
                x = stations[i][3]
                index = i
        return stations[index]

def rsort_x(a):
    for i in range(len(a) - 1):
        maxi = i
        for j in range(i, len(a)):
            if float(a[j][0]) > float(a[maxi][0]):
                maxi = j
        (a[i], a[maxi]) = (a[maxi], a[i])
    return a

def rsort_y(a):
    for i in range(len(a) - 1):
        maxi = i
        for j in range(i, len(a)):
            if float(a[j][1]) > float(a[maxi][1]):
                maxi = j
        (a[i], a[maxi]) = (a[maxi], a[i])
    return a

def sort_x(a):
    for i in range(1, len(a)):
        for j in range(i, 0, -1):
            if float(a[j][0]) < float(a[j - 1][0]):
                (a[j], a[j - 1]) = (a[j - 1], a[j])
    return a

def sort_y(a):
    for i in range(1, len(a)):
        for j in range(i, 0, -1):
            if float(a[j][1]) < float(a[j - 1][1]):
                (a[j], a[j - 1]) = (a[j - 1], a[j])
    return a

def FindingDots(x1,y1,x2,y2,x0,y0,r0):
    dots =[]
    if x1 != x2 and y1!= y2:
        k = (y1 - y2) / (x1 - x2)
        b = y1 - k * x1
        b1 = 2 * k * b - 2 * x0 - 2 * k * y0
        a1 = 1 + k**2
        for i in range(6):
            tek = []
            R = ComputingRadius(r0,i)
            c1 = x0**2 + y0**2 - R**2 + b**2 - 2*b*y0
            Discr = b1**2 - 4 * a1 * c1
            if Discr <= 0:
                pass
            else:
                x = (-b1 + math.sqrt(Discr)) / (2 * a1)
                y = k * x + b
                tek.append(x)
                tek.append(y)
                dots.append(tek)
                x = 0
                y = 0
                x = (-b1 - math.sqrt(Discr)) / (2 * a1)
                y = k * x + b
                tek = []
                tek.append(x)
                tek.append(y)
                dots.append(tek)
    elif x1 == x2:
        for i in range(6):
            tek = []
            R = ComputingRadius(r0, i)
            c1 =  (x0 - x1) ** 2 + y0 ** 2 - R ** 2
            Discr = 4 * y0 ** 2 - 4 * c1
            if Discr <= 0:
                pass
            else:
                y = (2 * y0 + math.sqrt(Discr)) / 2
                x = x1
                tek.append(x)
                tek.append(y)
                dots.append(tek)
                x = 0
                y = 0
                y = (2 * y0 - math.sqrt(Discr)) / 2
                x = x1
                tek = []
                tek.append(x)
                tek.append(y)
                dots.append(tek)
    elif y1 == y2:
        for i in range(6):
            tek = []
            R = ComputingRadius(r0, i)
            c1 =  (y0 - y1) ** 2 + x0 ** 2 - R ** 2
            Discr = 4 * x0 ** 2 - 4 * c1
            if Discr <= 0:
                pass
            else:
                x = (2 * x0 + math.sqrt(Discr)) / 2
                y = x1
                tek.append(x)
                tek.append(y)
                dots.append(tek)
                x = 0
                y = 0
                x = (2 * x0 - math.sqrt(Discr)) / 2
                y = x1
                tek = []
                tek.append(x)
                tek.append(y)
                dots.append(tek)
    if x1 < x2:
        x3 = x1
        x4 = x2
    else:
        x3 = x2
        x4 = x1
    if y1 < y2:
        y3 = y1
        y4 = y2
    else:
        y3 = y2
        y4 = y1
    new_dots = []
    for i in range(len(dots)):
        if dots[i][0] < x3 or dots[i][0] > x4 or dots[i][1] < y3 or dots[i][1] > y4 or dots[i] in new_dots:
            pass
        else:
            new_dots.append(dots[i])
    if x1 < x2:
        new_dots = sort_x(new_dots)
    elif x1 > x2:
        new_dots = rsort_x(new_dots)
    else:
        if y1 < y2:
            new_dots = sort_y(new_dots)
        elif y1 > y2:
            new_dots = rsort_y(new_dots)
    return new_dots

if __name__ == '__main__':
    import math
    colors = ['VIOLET','INDIGO','BLUE','GREEN','YELLOW','ORANGE','RED']
    stations, lines = init()
    x0 = lines[0][0]
    y0 = lines[0][1]
    current_station = ChoiseStation(stations, x0, y0)
    distance = SearchDistance(x0,y0,current_station[0],current_station[1])
    start_level = DefinitionLevel(current_station[2], distance)
    old_level = start_level
    print('Power on. CELL_ID:',current_station[3],', SIGNAL_LEVEL:',colors[start_level],sep = '')
    for i in range(len(lines)):
        if lines[i][0] < lines[i][2]:
            const = 0.01
        else:
            const = -0.01
        if lines[i][1] < lines[i][3]:
            const = 0.01
        else:
            const = -0.01
        dots = FindingDots(lines[i][0],lines[i][1],lines[i][2],lines[i][3],current_station[0],current_station[1],current_station[2])
        for j in range(len(dots)):
            distance = SearchDistance(dots[j][0] + const, dots[j][1], current_station[0], current_station[1])
            new_level = DefinitionLevel(current_station[2], distance)
            if j == -1:
                pass
            else:
                print('Signal changed. SIGNAL_LEVEL:',colors[new_level],sep = '')

        if colors[new_level] == 'RED':
            x_t = dots[len(dots) - 1][0] + const
            y_t = dots[len(dots) - 1][1]
            current_station = ChoiseStation(stations, x_t, y_t)
            distance = SearchDistance(x_t + const, y_t, current_station[0], current_station[1])
            newstart_level = DefinitionLevel(current_station[2], distance)
            print('Cell changed. CELL_ID:',current_station[3],', SIGNAL_LEVEL:',colors[newstart_level],sep = '')
            dots = FindingDots(x_t, y_t, lines[i][2], lines[i][3], current_station[0], current_station[1],current_station[2])
            for j in range(len(dots)):
                distance = SearchDistance(dots[j][0], dots[j][1], current_station[0], current_station[1])
                new_level = DefinitionLevel(current_station[2], distance )
                print('Signal changed. SIGNAL_LEVEL:', colors[new_level], sep='')