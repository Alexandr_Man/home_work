import socket
import queue
from threading import Thread

q_input1 = queue.Queue() #делаем очереди
q_input2 = queue.Queue()
q_output1 = queue.Queue()
q_output2 = queue.Queue()

def inputer1(): #инпутер от первого клиента
    while True:
        data = conn1.recv(1024)
        data_s = data.decode()
        q_output2.put(data_s)

def inputer2():
    while True:
        data = conn2.recv(1024)
        data_s = data.decode()
        q_output1.put(data_s)

def outputer1(): #аутпутер от первого клиента
    while True:
        data_s = q_output1.get()
        conn1.send(data_s.encode())

def outputer2():
    while True:
        data_s = q_output2.get()
        conn2.send(data_s.encode())

sock1 = socket.socket() #делаем связь с обоими клиентами
sock1.bind(('', 9090))
sock1.listen(1)
conn1, addr1 = sock1.accept()

sock2 = socket.socket()
sock2.bind(('', 9091))
sock2.listen(1)
conn2, addr2 = sock2.accept()

thread_input_1 = Thread(target=inputer1) #создаем процессы
thread_input_2 = Thread(target=inputer2)
thread_output_1 = Thread(target=outputer1)
thread_output_2 = Thread(target=outputer2)

thread_input_1.start() #запускаем процессы
thread_input_2.start()
thread_output_1.start()
thread_output_2.start()

thread_input_1.join() #останавливаем процессы
thread_input_2.join()
thread_output_1.join()
thread_output_2.join()

conn1.close() #закрываем соединения с клиентами
conn2.close()