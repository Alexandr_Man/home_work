def N(x1,x2,x3):
    if x2 == x3:
        k = 1
    else:
        k = 0
    if x1 == 1 and k == 0:
        return 0
    else:
        return 1

def ili(z1,z2):
    if z1 == 1 or z2 == 1:
        return 1
    else:
        return 0
def iii(z1,z2):
    if z1 == 1 and z2 == 1:
        return 1
    else:
        return 0

def P(x1,x2,x3):
    k1 = iii(x1, x2)
    k2 = iii(x1, x3)
    k3 = iii(x2, x3)
    k12 = ili(k1, k2)
    return ili(k12, k3)

def B(x1,x2,x3):
    b1 = ili(x1, x2)
    b2 = ili(b1, x3)
    n1 = N(x1,x2,x3)
    n2 = N(x2,x1,x3)
    n3 = N(x3,x1,x2)
    b3 = iii(b2,n1)
    b4 = iii(n2,n3)
    return iii(b3,b4)

mas = []
for i in range(8):
    a = str(bin(i))[2:]
    if len(a) < 3:
        a = '0' * (3 - len(a)) + a
    mas.append(a)
s = ''
for i in mas:
    a1 = int(i[0])
    a2 = int(i[1])
    a3 = int(i[2])
    s += str(B(a1,a2,a3)) + ' '
print(s)
