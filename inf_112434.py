n = int(input())
dict = {}
for i in range(n):
    a, b = map(int,input().split())
    s = b % a
    if s != 0:
        if s in dict.keys():
            dict[s] += 1
        else:
            dict[s] = 1
if len(dict) == 0:
    print(0)
else:
    m = 0
    ind = -1
    for i in dict:
        if dict[i] > m:
            m = dict[i]
            ind = i
        elif dict[i] == m:
            if i > ind:
                ind = i
    print(ind)