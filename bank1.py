import matplotlib.pyplot as plt
import random
print('Введите максимальное количество клиентов, входящих в минуту (4)')
P_max = int(input())
print('Введите минимальное и максимальное время, затрачиваемое на одного клиента (1,9)')
T_min, T_max = map(float,input().split())
print("Введите допустимое время ожидания (15)")
M = float(input())
L = 480 #Время работы системы в минутах
ind = 0
masx = []
masy = []
for k in range(1,1000): #Поиск минимального удовлетворяющего к
    if ind == 0:
        masx.append(k)
        N = 0  # Число клиентов в помещении банка
        bad_minutes = 0  # Счетчик плохих минут
        for i in range(1,L+1): #По минуточкам
            P  = random.randint(0,P_max) #Рандомное количество клиентов в минуту
            T = T_min + random.random() * (T_max - T_min) #Рандомное время обслуживания в эту минуту
            R = int(k/T)  #Количество ушедших в эту минуту
            N += P - R
            if N < 0:
                N = 0
            dT = N / (k * T) #Среднее время ожидания в очереди
            if dT > M:
                bad_minutes+=1
        masy.append(bad_minutes/L)
        if bad_minutes/L <= 0.05: #Нашли первый где плохих минут меньше 5%
            ind = 1
    else:
        break
figure = plt.figure()
plt.plot(masx,masy)
plt.show()
print(k-1)
#Потестив модель стало понятно что нужно от 7 до 8 касс
#Детерминированная модель с усредненными  P = 2, T = 5 требует 10 касс