mas = []
temp = ['.' for x in range(8)]
for i in range(8) :
    mas.append(temp.copy())
a = input()
string = a[0]
n = int(a[1])
dict = {
    'a':0,
    'b':1,
    'c':2,
    'd':3,
    'e':4,
    'f':5,
    'g':6,
    'h':7
}
dict1 = {
    1:7,
    2:6,
    3:5,
    4:4,
    5:3,
    6:2,
    7:1,
    8:0
}
m = dict[string]
n = dict1[n]
for i in range(8):
    for j in range(8):
        if i == n and j == m:
            mas[i][j] = 'K'
        if (i == n-1 or i == n+1) and (j == m-2 or j == m+2):
            mas[i][j] = '*'
        if (i == n-2 or i == n+2) and (j == m-1 or j == m+1):
            mas[i][j] = '*'
for i in range(len(mas)):
    v = ''
    for j in range(len(mas[i])):
        v += str(mas[i][j])
        v += ' '
    print(v)