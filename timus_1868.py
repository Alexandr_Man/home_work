def f(mas,a):
    for i in range(12):
        if mas[i] == a:
            return i
univ = []
for i in range(12):
    univ.append(input())
n = int(input())
mas = []
for i in range(n):
    k = int(input())
    s = 0
    for j in range(k):
        a = input()
        d = a.find(':')
        un = a[:d - 1]
        med = a[d + 2:]
        if un in univ:
            if med == 'gold' and f(univ, un) < 4:
                s += 1
            elif med == 'silver' and 4 <= f(univ, un) < 8:
                s += 1
            elif med == 'bronze' and 8 <= f(univ, un):
                s += 1
    mas.append(s)

m = max(mas)
g = 0
for i in range(len(mas)):
    if mas[i] == m:
        g += 1
print(g * 5)