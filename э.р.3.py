def init_1(): #Ввод цифер
    n,m,g = map(int,input().split())
    return n,m,g

def init_2(n): #Ввод букв
    mas = []
    for i in range(n):
        a = list(input())
        mas.append(a)
    return mas

def ratio(x): #Сопоставление сторон света и направлений
    dict = {
        's':'d',
        'S':'D',
        'n':'u',
        'N':'U',
        'e':'r',
        'E':'R',
        'w':'l',
        'W':'L'
    }
    y = dict[x]
    return y

def rename(a): #Переименовка
    for i in range(len(a)):
        for j in range(len(a[i])):
            a[i][j] = ratio(a[i][j])
    return a

def prosto(mas): #Создание массива идентичного буквенному, но состоящего из нулей
    new_mas = []
    for i in range(len(mas)):
        a = [0] * len(mas[i])
        new_mas.append(a)
    return new_mas

def step(i,j,mas_0,mas): # Функция шага
    w = {
        'd': {"n":0, "m":1, "j":1, "i":0}, #Шаг вниз n _ пре
        'u': {"n":0, "m":-1, "j":-1, "i":0}, #вверх
        'w': {'n':-1, 'm':0, 'i':-1, 'j':0},
        'e': {'n':1, 'm':0, 'i':1, 'm':0}
    }
    if mas[i][j] in w.keys():
        n = i + w[mas[i][j]]["n"]
        m = j + w[mas[i][j]]["m"]
        if mas_0[n][m] == 0:
            j = j + w[mas[i][j]]["j"]
            i = i + w[mas[i][j]]["i"]
        return i,j
    return False, False

def mine(mas,mas_0):
    mas_characteristics = []
    mas_sopostavitel = []
    time = 0
    for i in range(len(mas)):
        for j in range(len(mas[i])):
            time += 10000
            if mas_0[i][j] == 0:
                ind = 0
                while ind != 1:
                    time += 1
                    n,m = step(i,j,mas_0,mas)
                    if (n,m) == (False, False):


def finding_cycle(mas):
    mas_0 = prosto(mas)
    print(sum(mine(mas,mas_0)))

n,m,g = init_1()
mas = init_2(n)
mas = rename(mas)
finding_cycle(mas)