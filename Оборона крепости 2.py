def summ(mas): #Считает количество нападающих
    n = 0
    for i in range(len(mas)):
        n+=mas[i][0]
    return n

def sort_col(i): #Для сортировки матрицы
    return i[1]

n, s = map(int,input().split())
mas = []

for i in range(n):
    a = list(map(int,input().split())) #Делаем двумерный массив [[кол-во нападающих],[коэффициент стены] . . . ]
    if a[0] > 0:
        mas.append(a)

mas.sort(key = sort_col) #Сортируем по коэффициентам стен
mas.reverse()

for i in range(len(mas)):
    defenders = mas[i][0] // mas[i][1] #Для каждого участка считаем целочисленное кол-во зацитников
    if defenders == 0 and i != len(mas)-1 and s != 0:
        if mas[i][0] > mas[i+1][1] and mas[i][0] > mas:

            mas[i][0] = 0
            s -= 1
    elif s >= defenders:
        s -= defenders
        mas[i][0] -= (mas[i][1] * defenders)
        if mas[i][0] < 0:
            mas[i][0] = 0
    else:
        mas[i][0] -= (s * mas[i][1])
        if mas[i][0]<0:
            mas[i][0] = 0
        s = 0
    if s == 0:
        break

mas.sort()
mas.reverse()
new_mas = []

for i in range(len(mas)):
    if mas[i][0]!=0:
        new_mas.append(mas[i])

if s == 0:
    print(summ(mas))
elif s > len(new_mas):
    print(0)
else:
    print(summ(new_mas[s:]))


# 5 6
# 6 8
# 5 2
# 3 3
# 0 1
# 10 3