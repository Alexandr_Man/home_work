n = int(input())
a = input()
b = input()

max_s = [-1, -1]
for i in range(n):
    for j in range(n):
        if a[i] == b[j]:
            k = 0
            while (i + k < n) and (j + k < n) and (a[i + k] == b[j + k]):
                k += 1
            if k + 1 > len(max_s):
                max_s = a[i : i + k]
print(max_s)