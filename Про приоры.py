b = float(input()) #Плата на руки водителю без оформления
c, d = map(float, input().split()) #Стоимость страховки и на сколько она увеличивается каждый год
k, p = map(float, input().split()) #Сколько лет покупать олее дорогую страховку и процент увеличивания стоимости
cash_2, cash_1 = 0,b
k = int(k)
for i in range(1,k+1):
    cash_1 += c + d * i
    cash_2 += (c + d * i) * (1 + p / 100)
s = abs(cash_1 - cash_2)
if cash_1 < cash_2 and s > 0.000001:
    print('Cash')
else:
    print('Insurance')
a = str(round(s,6))
f = a.find('.')
if len(a) - f != 6:
    a+='0'*(6-len(a[f+1:]))
print(a)