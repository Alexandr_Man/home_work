mas = []
def func(massiv,n):
    mass = []
    for i in range(len(massiv)):
        if len(massiv[i]) != 2:
            pass
        else:
            first_word = massiv[i][0]
            if first_word == str(n):
                mass.append(' '.join(massiv[i]))
        # print(mass)
    return mass

with open("input.txt",encoding="KOI8-R") as f:
   text = f.read()
text_without_whitespace = text.strip()
lines = text_without_whitespace.split('\n')
for i in range(len(lines)):
   lines[i] = lines[i].split()
# print(lines)
for j in range(9,12):
    mass = func(lines,j)
    for i in range(len(mass)):
        mas.append(mass[i])

print('\n'.join(mas))