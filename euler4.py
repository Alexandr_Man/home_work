import matplotlib.pyplot as plt
def g(x):
    return 2*x

dx = 0.001
masx = []
masy = []
for i in range(2,11):
    masx.append(i)
    x = 1
    y = 1
    for j in range(int((i-1)/dx)+1):
        x += dx
        y += g(x - dx) * dx
    masy.append(abs((i**2 - y)/i**2))

fig = plt.figure()
plt.plot(masx,masy)
plt.show()
