from PIL import Image
import os
import queue
from threading import Thread
import numpy as np

def inputer(): #ф-я принимающая информацию о очередной картинке
    while True:
        a = input().split()
        if a[0] == 'exit':
            for i in range(n):
                q_input.put(None)
            break
        q_input.put(a)

def WhtBlck(dir): #ф-я делающая черно белым
    img = Image.open(dir).convert('RGB')
    x = np.array(img)
    for i in range(len(x)):
        for j in range(len(x[i])):
            r, g, b = x[i][j]
            S = int(r) + int(g) + int(b)
            if S > 382:
                x[i][j][0], x[i][j][1], x[i][j][2] = 255, 255, 255
            else:
                x[i][j][0], x[i][j][1], x[i][j][2] = 0, 0, 0
    img = Image.fromarray(x, 'RGB')
    return img

def Invrs(dir): #ф-я инверсии
    img = Image.open(dir).convert('RGB')
    x = np.array(img)
    for i in range(len(x)):
        for j in range(len(x[i])):
            r, g, b = x[i][j]
            x[i][j][0], x[i][j][1], x[i][j][2] = 255 - int(r), 255 - int(g), 255 - int(b)
    img = Image.fromarray(x, 'RGB')
    return img

def worker():
    while True:
        item = q_input.get()
        if item == None:
            q_output.put(None)
            break
        dir = item[0]
        func = int(item[1])
        owner = item[2]
        if func == 1:
            q_output.put([WhtBlck(dir), owner])
        else:
            q_output.put([Invrs(dir), owner])

def outputer():
    while True:
        item = q_output.get()
        exit_number = 0
        if item == None:
            exit_number += 1
        if exit_number == n:
            break
        if item != None:
            tek = os.getcwd()
            os.chdir(os.path.join(tek, item[1]))
            with open('number.txt', 'r') as f:
                number = int(f.read())
            with open('number.txt', 'w') as f:
                f.write(str(number + 1))
            item[0].save('pic_{}.jpg'.format(number))
            os.chdir(tek)

if __name__ == "__main__":
    q_input = queue.Queue()
    q_output = queue.Queue()
    name_mas = ['Vasya', 'Petya', 'Varya']
    base = os.getcwd()
    for i in range(len(name_mas)):
        try:
            os.mkdir(name_mas[i])
            os.chdir(name_mas[i])
            with open('number.txt', 'w') as f:
                f.write('0')
            os.chdir(base)
        except BaseException:
            pass

    n = 3  # кол-во процессов
    t1 = Thread(target=inputer)
    mas = []
    for i in range(n):
        mas.append(Thread(target=worker))
        mas[i].start()
    t3 = Thread(target=outputer)
    t1.start()
    t3.start()
    t1.join()
    t3.join()
    for i in range(n):
        mas[i].join()

# пример запуска
#picture.jpg 1 Vasya
#данные для запуска - картинка picture.jpg
# fixme нет данных для запуска.
# fixme, а лучше всего считывать задания из файла

"""
Основные части программы лучше убирать под 
if __name__ == "__main__":

Нет комментариев

Можно в комментариях указать пример запуска а в репозиторий положить
данные с которыми работать
"""
