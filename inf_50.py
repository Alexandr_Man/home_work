mas1 = list(map(int,input().split()))
mas2 = list(map(int,input().split()))
s = 0
win = ''
for i in range(10**6):
    dot1 = mas1.pop(0)
    dot2 = mas2.pop(0)
    if (dot1 > dot2 and (dot1 != 9 and dot2 != 0)) or (dot1 == 0 and dot2 == 9):
        mas1.append(dot1)
        mas1.append(dot2)
    elif (dot2 > dot1 and (dot2 != 9 and dot1 != 0)) or (dot2 == 0 and dot1 == 9):
        mas2.append(dot1)
        mas2.append(dot2)
    if len(mas1) == 0:
        win = 'second'
        win += ' ' + str(i+1)
        s = 1
        break
    elif len(mas2) == 0:
        win = 'first'
        win += ' ' + str(i + 1)
        s = 1
        break
if s == 0:
    print('botva')
else:
    print(win)