import queue
from threading import Thread

def inputer(): #функция ввода
    while True:#цикл ждет входа до тех пор пока не получит команду на выход (exit)
        a = input().split()
        if a[0] == 'exit':
            for i in range(n):#пихаем в очередь None столько же сколько потоков чтобы завершить каждый
                q_input.put(None)
            break
        q_input.put(a)#если все норм то пихаем массив [чичло, знак, число]

def worker():#основная функция работы
    while True:
        item = q_input.get()
        if item == None:#если нашли None то просто пихаем его во вторую очередь и обрываем цикл
            q_output.put(None)
            break
        answer = item[0]#если не None то обрабатываем и пихаем во вторую очередь результат
        if item[1] == '+':
            q_output.put(int(answer) + int(item[2]))
        else:
            q_output.put(int(answer) - int(item[2]))

def outputer():#ф-я вывода
    exit_number = 0#здесь считаем количесво None и когда их столько же сколько потоков, то все обрываем
    while True:
        item = q_output.get()
        if item == None:
            exit_number += 1
        if exit_number == n:
            break
        if item != None:#Если число то пишем его
            print(item)

if __name__ == "__main__":
    q_input = queue.Queue()
    q_output = queue.Queue()#делаем специальные очереди
    n = 3#фиксируем кол-во процесоов
    t1 = Thread(target=inputer)#создаем поток ввода
    mas = []
    for i in range(n):#создаем потоки - воркеры
        mas.append(Thread(target=worker))
        mas[i].start()
    t3 = Thread(target=outputer)#создаем поток вывода
    t1.start()
    t3.start()
    t1.join()
    t3.join()#все потоки запускаем и обрываем  # fixme запускаешь ты их не здесь 
    for i in range(n):
        mas[i].join()
    

