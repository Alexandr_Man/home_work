import requests as r
from bs4 import BeautifulSoup as Bs
import pickle
import os

s1 = "https://www.kinopoisk.ru/film/forma-vody-2017-977743/"
s1 += 'cast/'

if os.path.exists("html.pickle"):
    with open("html.pickle", "rb") as f:
        content = pickle.load(f)
else:
    page = r.get(s1)
    if page.status_code == 200:
        content = page.content.decode("utf-8")
        # print(content)

        with open("html.pickle", 'wb') as f:
            pickle.dump(content, f)
    else:
        print(page.status_code)

h = Bs(content, 'html.parser')
print(h)
for el in h.find_all(attrs={'id': "dub"}):
    for div in el.find_all(attrs={'class': "name"}):
        print(div.a.text, div.a.get('href'))