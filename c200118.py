import socket

sock = socket.socket()
sock.connect(('localhost', 9090))

while True:
    string = input()
    sock.send(string.encode())
    data = sock.recv(1024)
    if string == 'exit':
        break
    print(data.decode())

sock.close()