from collections import Counter
a = input()
a = a[:a.find('.')]
a = a.split()
a = ''.join(a)
a = a.upper()
mas = Counter(a).most_common(len(a))
m = -1
val = 'Z'
for i in mas:
    if i[0] < val and i[1] >= m:
        val = i[0]
        m = i[1]
print(val, m)