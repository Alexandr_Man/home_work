import matplotlib.pyplot as plt
def g(x):
    return 2*x

dx = 0.1
masx = [0.1,0.2,0.3,0.4,0.5,0.6,0.7]
masy = []
for i in range(7):
    y = 1
    x = 1
    for j in range(int(4/dx)+1):
        y1 = x**2
        y += g(x-dx) * dx
        x+=dx
    dx+=0.1
    masy.append(abs(y1-y))
figure1 = plt.figure()
plt.plot(masx,masy)


dx = 0.1
masx = [0.1, 0.01, 0.001, 0.0001, 0.0001]
masy = []
for i in range(5):
    y = 1
    x = 1
    for j in range(int(4/dx)+1):
        y1 = x**2
        y += g(x-dx) * dx
        x+=dx
    dx*=0.1
    masy.append(abs(y1-y))
figure2 = plt.figure()
plt.plot(masx,masy)
plt.show()

