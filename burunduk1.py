import matplotlib.pyplot as plt
N,M = 10,20 #Начальная численность видов
Ln, Lm = 70,50 #Максимальные численности
Kn, Km = 0.7,0.7 #Коэффициенты прироста
Dn, Dm = 0.1,0.1 #Коэфф взаимного влияния (# смерти)
mas_x_N,mas_x_M,mas_y_N,mas_y_M = [0],[0],[N],[M]
figure = plt.figure()
for i in range(1,21):
    mas_x_N.append(i) # Ось иксов - года
    mas_x_M.append(i)
    M1 = M
    N1 = N
    N *= (1 + (Kn * (Ln - N1) / Ln))
    N -= (Dn * M1)
    N = int(N)
    M *= (1 + (Km * (Lm - M1) / Lm))
    M -= (Dm * N1)
    M = int(M) #Итерационно считаем обе популяции
    mas_y_M.append(M)
    mas_y_N.append(N) #Ось игреков - количество особей
    # print(N,M)
plt.plot(mas_x_N,mas_y_N)
plt.plot(mas_x_M,mas_y_M)
plt.show()

#Видно что равновесие ниже чеме макчимум из-за того  что виды взаимодействуют, они система