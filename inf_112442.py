from collections import Counter
k = input() # ФИО
n = int(input())
mas = [] # Подходящие варианты
for i in range(n):
    a = input().split()
    if k == (a[0][0] + a[1][0] + a[2][0]):
        mas.append(' '.join(a))
list = Counter(mas).most_common() #Список с частотами
k = 100000000
if len(list) == 1:
    print(list[0][0], list[0][1])
else:
    for i in range(len(list)):
        for j in range(len(list)):
            if list[i][0] < list[j][0] and list[i][1] == list[j][1]:
                list[i], list[j] = list[j], list[i]
    for i in list:
        print(i[0], i[1])