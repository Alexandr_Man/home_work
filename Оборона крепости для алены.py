n, s = map(int, input().split())
wall = []
for i in range(n):
    a,k = map(int, input().split())
    if a >= k:
        wall.append([k,k*(a//k)])
    wall.append([a%k,a%k])
wall.sort(reverse = True)
attacker = 0
for k,a in wall:
    if k!= 0:
        defender = min(s,a//k)
        s -= defender
        attacker += (a-defender*k)
print(attacker)