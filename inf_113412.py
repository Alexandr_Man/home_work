n = int(input())
mas = []
for i in range(n):
    mas.append(input().split())
list = input().split()
mas_1 = []
mas_2 = []
for i in range(len(list)):
    dict = {}
    kol = 0
    for j in range(len(mas)):
        if list[i] in mas[j]:
            kol += 1
            for k in range(len(mas[j])):
                if mas[j][k] != list[i] and mas[j][k] not in dict.keys():
                    dict[mas[j][k]] = 1
                elif mas[j][k] != list[i] and mas[j][k] in dict.keys():
                    dict[mas[j][k]] += 1
    for m in dict:
        if dict[m] == kol:
            mas_1.append(m)
    per = kol / 2
    for m in dict:
        if dict[m] >= per:
            mas_2.append(m)


for i in range(len(mas_1)):
    dict = {}
    kol = 0
    for j in range(len(mas)):
        if mas_1[i] in mas[j]:
            kol += 1
            for k in range(len(mas[j])):
                if mas[j][k] != mas_1[i] and mas[j][k] not in dict.keys():
                    dict[mas[j][k]] = 1
                elif mas[j][k] != mas_1[i] and mas[j][k] in dict.keys():
                    dict[mas[j][k]] += 1
    per = kol / 2
    for m in dict:
        if dict[m] >= per:
            mas_2.append(m)
l = []
for i in range(len(mas_2)):
    if mas_2[i] not in mas_1 and mas_2[i] not in list and mas_2[i] not in l:
        l.append(mas_2[i])

print(' '.join(mas_1))
print(' '.join(l))