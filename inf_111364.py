n,m = map(int,input().split())
mas = []
temp = ['.' for x in range(m)]
for i in range(n):
    mas.append(temp.copy())

for i in range(len(mas)):
    if i%2==0:
        for j in range(len(mas[i])):
            if j%2==0:
                mas[i][j]='.'
            else:
                mas[i][j]='*'
    else:
        for j in range(len(mas[i])):
            if j%2==0:
                mas[i][j]='*'
            else:
                mas[i][j]='.'

for i in range(n):
    for j in range(m):
        print(mas[i][j], end=' ')
    print()
