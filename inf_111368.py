n = int(input())
mas = []
for i in range(n):
    mas.append(list(map(int, input().split())))
def IsSymmetric(A):
    b = 0
    for i in range(len(A)):
        if b == 0:
            for j in range(len(A)):
                if A[i][j] == A[j][i]:
                    pass
                else:
                    b = 1
                    break
        else:
            break
    return b
if IsSymmetric(mas) == 0:
    print("YES")
else:
    print("NO")
