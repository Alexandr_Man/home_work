def summ(mas): #Считает количество нападающих
    n = 0
    for i in range(len(mas)):
        n+=mas[i][0]
    return n
def sort_col(i): #Для сортировки матрицы
    return i[1]
n, s = map(int,input().split())
mas = []
for i in range(n):
    a = list(map(int,input().split())) #Делаем двумерный массив [[кол-во нападающих],[коэффициент стены] . . . ]
    if a[0] > 0:
        mas.append(a)
new_mas = []
for i in range(len(mas)):
    a = []
    b = []
    a.append(mas[i][1] * ( mas[i][0] // mas[i][1]))
    a.append(mas[i][1])
    b.append(mas[i][0] % mas[i][1])
    b.append(mas[i][0] % mas[i][1])
    if a[0] != 0:
        new_mas.append(a)
    if b[0] != 0:
        new_mas.append(b)
new_mas.sort(key = sort_col) #Сортируем по коэффициентам стен
new_mas.reverse()
sum = 0
for i in range(len(new_mas)):
    d = new_mas[i][0] // new_mas[i][1]
    if s >= d:
        s -= d
    elif 0 <= s < d:
        sum += (d - s) * new_mas[i][1]
        s = 0
print(sum)

