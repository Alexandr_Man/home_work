stack = []
a = input()
while a != 'exit':
    a = a.split()
    if a[0] == 'pop':
        print(stack[len(stack)-1])
        stack = stack[:len(stack)-1]
    elif a[0] == 'back':
        print(stack[len(stack)-1])
    elif a[0] == 'size':
        print(len(stack))
    elif a[0] == 'clear':
        print('ok')
        stack.clear()
    elif a[0] == 'push':
        stack.append(a[1])
        print('ok')
    a = input()
 
print('bye')
 
