import matplotlib.pyplot as plt
N0 = 100
k = 0.5
L = 1000
R = 40

masx = []
masy = []
n = N0
for i in range(14):#Модель неограниченного роста
    masx.append(i)
    masy.append(int(n))
    n*=(k+1)
figure = plt.figure()
plt.plot(masx,masy)

masx = []
masy = []
n = N0
for i in range(14):#Модель ограниченного роста
    masx.append(i)
    ki = (L-n)/L*k
    masy.append(int(n))
    n *= (1+ki)
plt.plot(masx,masy)

masx = []
masy = []
n = N0
for i in range(14):#Модель ограниченного роста c отловом
    masx.append(i)
    ki = (L-n)/L*k
    masy.append(int(n))
    n = (1+ki)*n - R
plt.plot(masx,masy)
plt.show()

#Графики наглядно показывают что неограниченный рост просто идет по показательному графику
#Ограниченный потихоньку приближвется к границе и на ней установливается
#С отловом возрастает еще плавнее чем с ограничением, но граница устанавливается немного ниже уста7нговленной границы