n = int(input())
mas = []
for i in range(n):
    mas.append(list(map(int, input().split())))


def Transpose(a):
    n = len(a)
    b = 0
    for i in range(n):

        for j in range(n - b):
            z = a[i][j + b]
            a[i][j + b] = a[j + b][i]
            a[j + b][i] = z
        b += 1
    return a


mas1 = Transpose(mas)
for i in range(n):
    mas1[i].reverse()
for i in range(len(mas1)):
    v = ''
    for j in range(len(mas1[i])):
        v += str(mas1[i][j])
        v += ' '
    print(v)