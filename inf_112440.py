n = int(input())
a1 = ['', -1]
a2 = ['', -1]
a3 = ['', -1]
for i in range(n):
    a = input().split()
    name = a[1]
    score = int(a[0])
    if score > a1[1]:
        if name == a1[0]:
            a1 = [name, score]
        elif name == a2[0]:
            a2 = a1
            a1 = [name, score]
        else:
            a3 = a2
            a2 = a1
            a1 = [name,score]
    elif score > a2[1]:
        if name == a2[0]:
            a2 = [name, score]
        elif name != a1[0]:
            a3 = a2
            a2 = [name, score]
    elif score > a3[1]:
        if name != a1[0] and name != a2[0]:
            a3 = [name, score]

print(1,' place. ', a1[0], '(', a1[1], ')', sep = '')
print(2,' place. ', a2[0], '(', a2[1], ')', sep = '')
print(3,' place. ', a3[0], '(', a3[1], ')', sep = '')