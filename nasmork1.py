import matplotlib.pyplot as plt
from math import ceil
mas = []
mas_x = []
mas_y1 = []
mas_y2 = []
L = 1000 #Общее число жителей
K = 0.5 #Коэффициент заболевания
N = 1 #Число больных
W = 0 #Число переболевших
kol_zabolevshih = 1
for i in range(7): #Первая фаза - пока никто не выздоравливает
    Z = ceil(K * (L - N) / L * N)
    N += Z
    mas.append(Z)
    mas_y1.append(Z)
    mas_y2.append(N)
    kol_zabolevshih += Z
    print(mas)
while mas[0] != 0: #Вторая фаза - пока все не выздоровеют
    V = mas.pop(0)
    Z = ceil(K * (L - N - W) / L * N)
    W += V
    N += Z - V
    mas.append(Z)
    mas_y1.append(Z)
    mas_y2.append(N)
    kol_zabolevshih += Z
    print(mas)
print(max(mas_y1)) #Максимальное количество заболевших в один день
print(max(mas_y2)) #Максимальное число больных в один день
for i in range(len(mas_y1)):
    mas_x.append(i) #генерим список дней
print(mas_x[len(mas_x)-1],mas_x[len(mas_x)-1]-7) #Первое число - день когда выздоровеет последний больной, второе - когда заболел последний человек
print(kol_zabolevshih) #Количество переболевших. Как оказалось, переболели все
fig1 = plt.figure() #Первый график - количество заболевших в день от дня
plt.plot(mas_x,mas_y1)
plt.show()
fig2 = plt.figure() #Второй график - количество больных в день от дня
plt.plot(mas_x,mas_y2)
plt.show()