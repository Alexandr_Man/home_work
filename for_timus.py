n = int(input())
temp = []
for i in range(n):
    mas = input().split()
    if mas[0] == 'register':
        if len(temp) == 0:
            mas = mas[1:]
            mas.append(0)
            temp.append(mas)
            print('success: new user added')
        else:
            for i in range(len(temp)):
                if mas[1] == temp[i][0]:
                    print('fail: user already exists')
                    break
                else:
                    mas = mas[1:]
                    mas.append(0)
                    temp.append(mas)
                    print('success: new user added')
    elif mas[0] == 'login':
        ind = -1
        for i in range(len(temp)):
            if temp[i][0] == mas[1]:
                if temp[i][1] == mas[2]:
                    if temp[i][2] == 0:
                        print('success: user logged in')
                        ind+=1
                        temp[i][2] = 1
                    else:
                        print('fail: already logged in')
                elif temp[i][1] != mas[2]:
                    print('fail: incorrect password')
                    ind+=1
        if ind == -1:
            print('fail: no such user')
    else:
        ind = -1
        for i in range(len(temp)):
            if temp[i][0] == mas[1]:
                if temp[i][2] == 1:
                    print('success: user logged out')
                    ind += 1
                    temp[i][2] = 0
                else:
                    print('fail: already logged out')
                    ind+=1
        if ind == -1:
            print('fail: no such user')