def summ(mas):
    n = 0
    for i in range(len(mas)):
        n+=mas[i]
    return n
n, s = map(int,input().split())
sum = 0
mas = []
for i in range(n):
    a,k = map(int,input().split())
    if k == 2:
        if a % 2 == 0 and a > 0:
            while a != 0 and s > 0:
                a-=2
                s-=1
        elif a % 2 == 1 and a > 1:
            while a != 1 and s > 0:
                a-=2
                s-=1
    mas.append(a)
if s == 0:
    print(summ(mas))
else:
    d = summ(mas) - s
    if d < 0:
        d = 0
    print(d)

