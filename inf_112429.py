n = int(input())
m = 100000
mas = []
for i in range(n):
    a = input().split()
    a[2] = a[2].split('.')
    a = [a[0],a[1], int(a[2][0]), int(a[2][1]), int(a[2][2])]
    mas.append(a)
    if a[4] < m:
        m = a[4]

mas1 = []
for i in mas:
    if i[4] == m:
        mas1.append(i)

mas2 = []
m = 50
for i in mas1:
    if i[3] < m:
        m = i[3]
for i in mas1:
    if i[3] == m:
        mas2.append(i)

mas3 = []
m = 50
for i in mas2:
    if i[2] < m:
        m = i[2]
for i in mas2:
    if i[2] == m:
        mas3.append(i)

if len(mas3) == 1:
    a1 = str(mas3[0][2])
    a2 = str(mas3[0][3])
    a3 = str(mas3[0][4])
    a1 = '0' * (2 - len(a1)) + a1
    a2 = '0' * (2 - len(a2)) + a2
    print(mas3[0][0], mas3[0][1], a1+'.'+a2+'.'+a3, sep=' ')
else:
    print(len(mas3))