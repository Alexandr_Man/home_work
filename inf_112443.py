a = input()
a = a[:a.find('.')]
mas = []
dict = {}
mn = set()
for i in range(len(a)):
    if a[i].isalpha() and a[i].islower():
        if a[i] not in mn:
            mn.add(a[i])
            mas.append(a[i])
            dict[a[i]] = 1
        else:
            dict[a[i]] += 1
mas.sort()
for i in mas:
    print(i, dict[i])