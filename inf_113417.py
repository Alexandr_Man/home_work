a = list(map(int, input()))
b = list(map(int, input()))
k = 0;
if len(a) < len(b):
    a, b = b, a
for i in range(len(b)):
    a[i] = a[i] + b[i]

for i in range(len(b) - 1, -1, -1):
    a[i] += k
    k = a[i] // 10
    a[i] = a[i] % 10
if k:
    a = [1] + a
a = ''.join(map(str, a))
print(a)