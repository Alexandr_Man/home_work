n, m = map(int, input().split())
mas = []
for i in range(n):
    mas.append(list(input().split()))
x, y = map(int, input().split())

if x != y:
    for i in range(n):
        for j in range(m):
            if j == x:
                a = mas[i][j]
            elif j == y:
                b = mas[i][j]
        for g in range(m):
            if mas[i][g] == a:
                mas[i][g] = b
            elif mas[i][g] == b:
                mas[i][g] = a

for i in range(n):
    print(' '.join(mas[i]))