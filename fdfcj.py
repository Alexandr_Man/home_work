a, b, c = map(int, input().split())
def D(a, b, c):
    return b**2 - 4*a*c
if a == 0 and b == 0 and c != 0:
    print(0)
elif a == 0 and b == 0 and c == 0:
    print(-1)
elif a == 0 and b != 0 and c == 0:
    print(1)
    print(0)
elif a == 0 and b != 0 and c != 0:
    print(1)
    print(round(-c/b, 6))
else:
    d = D(a,b,c)
    if d == 0:
        print(1)
        print(round(-b/(2*a), 6))
    elif d < 0:
        print(0)
    else:
        print(2)
        x1,x2 = round((-b+d**0.5)/(2*a), 6), round((-b-d**0.5)/(2*a), 6)
        print(min(x1,x2))
        print(max(x1,x2))
