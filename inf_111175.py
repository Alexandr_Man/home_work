a = list(map(int,input().split()))
a_1 = a[0]
a_2 = a[1]
a_3 = 0
a_4 = 0
for i in range(len(a)):
    if a[i] > a_1:
        a_2 = a_1
        a_1 = a[i]
    elif a[i] > a_2:
        a_2 = a[i]
    if a[i] < a_3:
        a_4 = a_3
        a_3 = a[i]
    elif a[i] < a_4:
        a_4 = a[i]
if a_3 > a_4:
    a_3,a_4 = a_4,a_3
if a_1 > a_2:
    a_1,a_2 = a_2,a_1
if a_3 * a_4 > a_1 * a_2:
    print(a_3, a_4)
else:
    print(a_1,a_2)
