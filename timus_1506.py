n, k = map(int,input().split())
mas = list(map(int,input().split()))
kol = n // k
ost = n % k
if ost != 0:
    kol += 1
matrix = []
for i in range(kol):
    s = i
    a = []
    for j in range(k + 1):
        if s + kol * j +1 <= len(mas):
            a.append(mas[s + kol * j])
        else:
            break
    matrix.append(a)

for i in range(len(matrix)):
    s = ''
    for j in range(len(matrix[i])):
        matrix[i][j] = str(matrix[i][j])
        d = 4 - len(matrix[i][j])
        r = ' ' * d
        r += matrix[i][j]
        s += r
    print(s)