stack = []
a = input()
while a != 'exit':
    a = a.split()
    if a[0] == 'pop':
        if len(stack) != 0:
            print(stack[0])
            stack = stack[1:]
        else:
            print('error')
    elif a[0] == 'front':
        if len(stack) != 0:
            print(stack[0])
        else:
            print('error')
    elif a[0] == 'size':
        print(len(stack))
    elif a[0] == 'clear':
        print('ok')
        stack.clear()
    elif a[0] == 'push':
        stack.append(a[1])
        print('ok')
    a = input()
 
print('bye')
