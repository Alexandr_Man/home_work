n = int(input())
mas = []
temp = ['.' for x in range(n)]
for i in range(n):
    mas.append(temp.copy())

for i in range(n):
    mas[i][i] = '*'
    mas[n - i - 1][i] = '*'
    mas[i][(n) // 2] = '*'
    mas[(n) // 2][i] = '*'
for i in range(n):
    for j in range(n):
        print(mas[i][j], end=' ')
    print()
