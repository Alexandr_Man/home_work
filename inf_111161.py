def sort(a):
    for j in range(1, len(a)):
        for i in range(len(a)-j):
            if int(a[i][1]) < int(a[i+1][1]):
                a[i],a[i+1] = a[i+1],a[i]
    return a
 
n = int(input())
g = []
for i in range(n):
    a = input()
    a=a.split()
    g.append(a)
g = sort(g)
 
for i in range(len(g)):
    print(g[i][0])
