def g(x):
    return 2*x+3

if __name__ == '__main__':
    x=0
    y=0
    dx = 0.01
    for i in range(501):
        s = []
        s.append(str(round(x,2)))
        s.append(str(round(y,2)))
        s.append(str(round(g(x),2)))
        s.append(str(round(x**2+3*x-4,2)))
        print(' '.join(s))
        x+=dx
        y += g(x-dx) * dx