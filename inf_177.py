def f(mas,a):
    m = 10000
    q1=-1
    q2=-1
    q3=-1
    h = mas[a][:a]
    for i in range(len(h)-1):
        for j in range(i+1,len(h[i+1:])):
            s = 0
            s+=mas[a][i]
            s+=mas[a][j]
            s+=mas[i][j]
            if s<m:
                m = s
                q1=a
                q2=i
                q3=j
    return m,q1,q2,q3

n = int(input())
mas = []
for i in range(n):
    mas.append(list(map(int,input().split())))
min = 10000
m1= -1
m2 = -1
m3 = -1
for u in range(2,n):
    c1,c2,c3,c4 = f(mas,u)
    if c1<min:
        min = c1
        m1 = c2
        m2 = c3
        m3 = c4
mas1=[m1+1,m2+1,m3+1]
mas1.sort()
mas1[0] = str(mas1[0])
mas1[1] = str(mas1[1])
mas1[2] = str(mas1[2])
print(' '.join(mas1))