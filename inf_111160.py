def sort(a):
    for j in range(1, len(a)-1):
        for i in range(len(a)-j):
            if a[i] > a[i+1]:
                a[i],a[i+1] = a[i+1],a[i]
    return a
 
n = int(input())
a = list(map(int,input().split()))
a = sort(a)
if n in a:
    z = 1
else:
    z=0
for i in range(len(a)):
    if a[i]-n >= 3:
        z+=1
        n = a[i]
print(z)
 
