import sqlite3
conn = sqlite3.connect('products.db')
c = conn.cursor()


drop_table_product = '''
DROP TABLE IF EXISTS product;
'''

create_table_product = '''
CREATE TABLE product(
name TEXT
price FLOAT
);
'''

list_table_name = '''
SELECt name FROM sqlite_master WHERE type='table'
'''

c.execute(drop_table_product)
c.execute(create_table_product)
conn.commit()

for row in c.execute(list_table_name):
    print(row)

conn.close()