n = int(input())
mas = []
masx = []
for i in range(n):
    a = list(map(int,input().split()))
    mas.append(a)
    if a[1] == 0:
        masx.append(a)
if len(masx) < 2:
    print(0)
else:
    mini = 1000000000
    mini_i = -1
    maxi_i = -1
    maxi = -100000000
    for i in range(len(masx)):
        if masx[i][0] < mini:
            mini = masx[i][0]
            mini_i = i
        if masx[i][0] > maxi:
            maxi = masx[i][0]
            maxi_i = i
    A = masx[maxi_i]
    B = masx[mini_i]
    m = -1000000
    ind = -1
    for i in range(len(mas)):
        if abs(mas[i][1]) > m:
            m = abs(mas[i][1])
            ind = i
    C = mas[ind]
    a = 0.5 * (A[0] - B[0])
    print(abs(a * C[1]))