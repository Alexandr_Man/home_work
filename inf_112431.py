n = int(input())
mas = []
min15 = 6000
min20 = 6000
min25 = 6000
for i in range(n):
    a = input().split()
    a[2] = int(a[2])
    a[3] = int(a[3])
    mas.append(a)
    if a[2] == 15 and a[3] < min15:
        min15 = a[3]
    elif a[2] == 20 and a[3] < min20:
        min20 = a[3]
    elif a[2] == 25 and a[3] < min25:
        min25 = a[3]

c1, c2, c3 = 0, 0, 0
for i in mas:
    if i[2] == 15 and i[3] == min15:
        c1 += 1
    elif i[2] == 20 and i[3] == min20:
        c2 += 1
    elif i[2] == 25 and i[3] == min25:
        c3 += 1
print(c1,c2,c3)