def sort(a):
    for j in range(1, len(a)-1):
        for i in range(len(a)-j):
            if a[i] > a[i+1]:
                a[i],a[i+1] = a[i+1],a[i]
    return a
 
n,k = map(int,input().split())
 
z = []
for i in range(k):
    z.append(int(input()))
 
if len(z)==2:
    if z[1]<z[0]:
        z[1],z[0]=z[0],z[1]
else:
    z = sort(z)
 
x = 0
for i in range(len(z)):
    x+=z[i]
    if x>n:
        break
   
if x>n:
    print(i)
else:
    print(i+1)
