from multiprocessing import Queue, Process
import sys
import os

def inputer(q_input, fileno):
    sys.stdin = os.fdopen(fileno)
    with open('file_for_multiprocessing.txt') as f: #открываем заранее сгенерированный файл с данными
        list = f.read().split()
        for i in range(len(list)):
            a = list[i]
            q_input.put(a) #забиваем в очередь

def worker(q_input, q_output):
    while True:
        item = q_input.get()
        q_output.put(int(item)**16000) #делаем тяжелую операцию 16000**16000

def outputer(q_output):
    while True:
        item = q_output.get()
        print(item) #все печатаем

if __name__ == '__main__':
    #сначала необходимо запустить программу list_creater
    q_input = Queue()
    q_output = Queue()
    fn = sys.stdin.fileno()
    n = int(input())
    t1 = Process(target=inputer, args=(q_input, fn))
    mas = []
    for i in range(n):
        mas.append(Process(target=worker, args=(q_input, q_output)))
        mas[i].start()
    t3 = Process(target=outputer, args=(q_output,))
    t1.start()
    t3.start()
    t1.join()
    t3.join()
    for i in range(n):
        mas[i].join()


"""
Мой цп не загрузился - задачи слишком мелкие
"""
