dict = {
    'a':1,
    1:'a',
    'b':2,
    2:'b',
    'c':3,
    3:'c',
    'd':4,
    4:'d',
    'e':5,
    5:'e',
    'f':6,
    6:'f',
    'g':7,
    7:'g',
    'h':8,
    8:'h',
    'i':9,
    9:'i',
    'j':10,
    10:'j',
    'k':11,
    11:'k',
    'l':12,
    12:'l',
    'm':13,
    13:'m',
    'n':14,
    14:'n',
    'o':15,
    15:'o',
    'p':16,
    16:'p',
    'q':17,
    17:'q',
    'r':18,
    18:'r',
    's':19,
    19:'s',
    't':20,
    20:'t',
    'u':21,
    21:'u',
    'v':22,
    22:'v',
    'w':23,
    23:'w',
    'x':24,
    24:'x',
    'y':25,
    25:'y',
    'z':26,
    26:'z'
}
a = input()
mas = []
for i in range(len(a)):
    mas.append(dict[a[i]])
for i in range(len(mas)-1, 0, -1):
    if mas[i]-mas[i-1] <= 0:
        mas[i] += 26
    mas[i] -= mas[i-1]

if mas[0]-5 <= 0:
    mas[0] += 26
mas[0] -= 6
mas1 = []
for i in range(len(mas)):
    mas1.append(dict[mas[i]+1])
mas1 = ''.join(mas1)
print(mas1)