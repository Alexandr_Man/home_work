from multiprocessing import Queue, Process
import sys
import os

def inputer(q_input, fileno, n): #функция ввода
    sys.stdin = os.fdopen(fileno)
    while True: #цикл ждет входа до тех пор пока не получит команду на выход (exit)
        a = input().split()
        if a[0] == 'exit':
            for i in range(n): #пихаем в очередь None столько же сколько процессов чтобы убить каждый
                q_input.put(None)
            break
        q_input.put(a) #если все норм то пихаем массив [чичло, знак, число]

def worker(q_input, q_output): #основная функция работы
    while True: #цикл просто ждет и работает
        item = q_input.get()
        if item == None: #если нашли None то просто пихаем его во вторую очередь и обрываем цикл
            q_output.put(None)
            break
        answer = item[0] #если не None то обрабатываем и пихаем во вторую очередь результат
        if item[1] == '+':
            q_output.put(int(answer) + int(item[2]))
        else:
            q_output.put(int(answer) - int(item[2]))

def outputer(q_output, n): #ф-я вывода
    exit_number = 0 #здесь считаем количесво None и когда их столько же сколько процессов, то все обрываем
    while True:
        item = q_output.get()
        if item == None:
            exit_number += 1
        if exit_number == n:
            break
        if item != None: #Если число то пишем его
            print(item)

if __name__ == '__main__':
    q_input = Queue()#делаем специальные очереди
    q_output = Queue()
    fn = sys.stdin.fileno()
    n = 3#фиксируем кол-во процесоов
    t1 = Process(target=inputer, args=(q_input, fn, n))#делаем процесс ввода
    mas = []
    for i in range(n):#создаем массив процессов воркеров и запускаем их
        mas.append(Process(target=worker, args=(q_input, q_output)))
        mas[i].start()
    t3 = Process(target=outputer, args=(q_output, n))#создаем аутпутера
    t1.start()
    t3.start()#запускаем процессы ввода и вывода
    t1.join()
    t3.join()
    for i in range(n):#все процессы останавливаем
        mas[i].join()
        
