a = int(input())
m1 = 0
m2 = 0
while a != 0:
    if a > m1 and a % 7 == 0 and a % 49 != 0:
        m1 = a
    if a > m2 and a % 7 != 0:
        m2 = a
    a = int(input())
if (m1 * m2) == 0:
    print(1)
else:
    print(m1 * m2)