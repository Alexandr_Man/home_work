def g(x):
    return 2*x

if __name__ == '__main__':
    x = 1
    y = 1
    dx = 0.1
    for i in range(11):
        s = []
        s.append(str(round(x,2)))
        s.append(str(round(y,2)))
        s.append(str(round(g(x),2)))
        s.append(str(round(x**2,2)))
        print(' '.join(s))
        x+=dx
        y += g(x-dx) * dx