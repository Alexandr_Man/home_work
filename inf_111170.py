def merge(a,b):
    j,i= 0,0
    mas = []
    for t in range(len(a)+len(b)+1):
        if a[i] <= b[j]:
            mas.append(a[i])
            i+=1
        elif a[i] >= b[j]:
            mas.append(b[j])
            j+=1
        if i == len(a):
            mas+=b[j:]
            break
        elif j == len(b):
            mas+=a[i:]
            break
    return mas
a = list(map(int,input().split()))
b = list(map(int,input().split()))
mas = merge(a,b)
v = ''
for i in range(len(mas)):
    v+=str(mas[i])+' '
print(v)